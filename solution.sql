-- To login to MySQL
mysql -u root -p

-- For creating a new databses
CREATE DATABASE blog_db;

-- for switching the active database
USE blog_db;


CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	Email VARCHAR(50) NOT NULL,
	Password VARCHAR(50) NOT NULL,
	`Datetime Created` DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	Title VARCHAR(50) NOT NULL,
	Content VARCHAR(50) NOT NULL,
	`Datetime Posted` DATETIME NOT NULL,
	PRIMARY KEY (id)
);


INSERT INTO users (Email, Password, `Datetime Created`) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (Email, Password, `Datetime Created`) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (Email, Password, `Datetime Created`) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (Email, Password, `Datetime Created`) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (Email, Password, `Datetime Created`) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

INSERT INTO posts (Title, Content, `Datetime Posted`) VALUES ("First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (Title, Content, `Datetime Posted`) VALUES ("Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (Title, Content, `Datetime Posted`) VALUES ("Third Code", "Welcom to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (Title, Content, `Datetime Posted`) VALUES ("Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");


SELECT * FROM posts WHERE id = 1;

SELECT Email, `Datetime Created` FROM users;

UPDATE posts SET Content = "Hello to the people of the Earth!" WHERE id = 2;

DELETE FROM users WHERE Email = "johndoe@gmail.com";


-- VALUES('Johnsmith@gmail.com',HASHBYTES('SHA1','PasswordA'),'2021-01-01 01:00:00')